var path = require('path');

const webpack = require('webpack');

const NODE_ENV = process.env.NODE_ENV || 'development';

const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: "./main.js",
    output: {
        path: path.join(__dirname, '../BEnd/public'),
        filename: "bundle.js"
        },
    module:{
        loaders:[
            {
                test: /.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query:{
                    presets:['es2015', 'react', 'stage-2']
                }
            }
        ]
    },
    plugins:[
        new CopyWebpackPlugin([{
            context:'static',
            from: '**/*',
            to: path.join(__dirname, '../BEnd/public')
        }])
    ],

    watch: NODE_ENV == 'development',

    watchOptions: {
        aggregateTimeout: 100
    },

    devtool: NODE_ENV == 'development' ? "cheap-inline-module-source-map" : null,

};