import React from 'react';
import ReactDOM from 'react-dom';
import {fetchTodos} from './api';

import configureStore from './utils/configureStore'
import {Root} from './react/components/Root'


const store = configureStore();

ReactDOM.render(
    <Root store = {store}/>,
    document.getElementById('app'));


