import {createStore, applyMiddleware} from 'redux';
import todoApp from '../react/reducers';

import promise from 'redux-promise';
import createLogger from 'redux-logger';


const configureStore = () =>{

    const middlewares = [promise];
    //TODO: webpack configuration to use variables
    if(process.env.NODE_ENV !== 'production') {
       middlewares.push(createLogger());
    }

    return createStore(todoApp, applyMiddleware(...middlewares));


}

export default configureStore;