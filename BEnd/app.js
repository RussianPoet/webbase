//If in c++ here should be main() function
var path = require('path');
var express = require('express');
var app = express();
var conf = require('./conf');


app.use(express.static(path.join(__dirname, 'public')));

app.listen(conf.port);

console.log('Server listening on port: ' + conf.port.toString());